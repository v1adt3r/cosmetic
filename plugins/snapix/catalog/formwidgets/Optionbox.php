<?php namespace Snapix\Catalog\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Config;
use Snapix\Catalog\Models\Color;

/**
 * optionbox Form Widget
 */
class Optionbox extends FormWidgetBase
{
    public function widgetDetails(){
        return[
            'name' => 'Optionbox',
            'description' => 'Field form adding sizes',
        ];
    }

    public function prepareVars(){
        $this->vars['id'] = $this->model->id;
        $this->vars['color'] = Color::with('product')->get();
    }

    public function render(){
        $this->prepareVars();
        return $this->makePartial('widget');
    }

    public function loadAssets()
    {
        $this->addCss('css/optionbox.css');
        $this->addJs('js/optionbox.js');
    }
    
}
