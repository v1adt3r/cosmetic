<?php namespace Snapix\Catalog\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Promo Codes Back-end Controller
 */
class PromoCodes extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Snapix.Catalog', 'catalog', 'promocodes');
    }
}
