<?php namespace Snapix\Catalog\Controllers;

use Backend;
use BackendMenu;
use Backend\Classes\Controller;
use Snapix\Catalog\Models\Order as OrderModel;
use Illuminate\Support\Facades\Input;

class Orders extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];
    
    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';



    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Snapix.Catalog', 'catalog', 'orders');

        $this->addCss('/plugins/snapix/catalog/assets/css/order.css');
        $this->addJs('/plugins/snapix/catalog/assets/js/order.js');

    }

    public function onDeliveryMethod()
    {
        $id = post('delivery_id');
        $model = $this->getOrderModel();

        $model->delivery_method_id = $id;
        $model->save();

        return $this->refreshOrderItemList();
    }

    public function onLoadUpdateItemForm()
    {
        $this->vars['id'] = post('id');
        $this->vars['name'] = post('name');
        $this->vars['qty'] = post('qty');
        $this->vars['model_id'] = post('model_id');

        return $this->makePartial('item_update_form');
    }

    protected function onLoadInfoProduct()
    {
        $id = post('product_id');
        $this->vars['product'] = \Snapix\Catalog\Models\Product::where('id', '=', $id)->first();
        return $this->makePartial('info_product');
    }

    protected function onUpdateItem()
    {
        $id = post('item_id');
        $model = $this->getOrderModel();
        $content = $model->contents;    
        $content[$id]['qty'] = post('qty_content');
        $content[$id]['total_price'] = $content[$id]['price'] * $content[$id]['qty'];
        $model->contents = $content;
        $model->save();
        return $this->refreshOrderItemList();
    }

    protected function onDeliteItem()
    {
        $id = post('item_id');
        $model = $this->getOrderModel();
        $content = $model->contents;
        unset($content[$id]);
        $model->contents = $content;
        $model->save();
        
        return $this->refreshOrderItemList();

    }

    protected function onAddProduct()
    {
        $id    = post('selected_product');
        $size  = post('selected_size');
        $color = post('selected_color');
        $qty   = post('qty_content');

        $product = \Snapix\Catalog\Models\Product::where('id', '=', $id)->first();
        $model   = $this->getOrderModel();
        $content = $model->contents;

        $content[$product->id]['id']          = $product->id;
        $content[$product->id]['name']        = $product->name;
        $content[$product->id]['price']       = $product->price;
        $content[$product->id]['total_price'] = (int)$qty * (int)$product->price;
        $content[$product->id]['size']        = $size;
        $content[$product->id]['color']       = $color;
        $content[$product->id]['qty']         = $qty;

        $model->contents = $content;
        $model->save();
    
        return $this->refreshOrderItemList();
    }

    protected function onAddItemForm()
    {
        $this->vars['products'] = \Snapix\Catalog\Models\Product::whereNotIn('id', post('ids'))->get();
        $this->vars['colors'] = \Snapix\Catalog\Models\Color::all();
        $this->vars['sizes'] = \Snapix\Catalog\Models\Size::all();
        $this->vars['model_id'] = post('model_id');

        return $this->makePartial('item_product_form');
    }
    


    protected function refreshOrderItemList()
    {
        $model = $this->getOrderModel();

        $contents = $model->contents;

        $sum = $model->delivery_method->price_change;
        foreach($model->contents as $k => $item):
            $sum += $item['total_price'];
        endforeach;

        if($model->promo_code != 0):
            $percent = \Snapix\Catalog\Models\PromoCode::where('id', '=', $model->promo_code)->first()->percent;
            $sum = $sum * $percent / 100;
        endif;

        $model->total_price = $sum;
        $model->save();

        $this->vars['contents'] = $contents;
        $this->vars['model_id'] = $model->id;
        $this->vars['total_price'] = $model->total_price;
        $this->vars['delivery_method'] = $model->delivery_method;
        $this->vars['promo_code'] = $model->promo_code;

        return ['#itemList' => $this->makePartial('item_list')];
    }


    protected function getOrderModel()
    {
        $model_id = post('list_id');
        $model = $model_id
        ? \Snapix\Catalog\Models\Order::where('id', '=', $model_id)->first()
        : new \Snapix\Catalog\Models\Order;

        return $model;
    }


}
