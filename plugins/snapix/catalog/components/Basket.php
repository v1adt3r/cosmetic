<?php namespace Snapix\Catalog\Components;

use Cms\Classes\ComponentBase;
use ApllicationException;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use RainLab\User\Facades\Auth;
use Snapix\Catalog\Models\Order;
use Snapix\Catalog\Models\DeliveryMethod;
use Snapix\Catalog\Models\PromoCode;
use  \Snapix\Catalog\Models\Product as ProductModel;
use \Snapix\Catalog\Models\Client as ClientModel;
use RainLab\User\Models\User;


class Basket extends ComponentBase
{

    public $products = [];
    public $prod = [];
    public $user_data = [];

    public $deliveryPrice;
    public $deliveryId;
    public $paymentId;
    public $totalPrice;
    public $cartCount;
    public $totalCount;
    public $promoCode;
    public $productSize;
    public $newMethods = [];
    public $deliveryTotalPrice;

    public function componentDetails()
    {
        return [
            'name'        => 'Basket Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }



    public function onRun(){
        $offerIds = Session::get('basket.product.offer_id', null);
        $offerQty = Session::get('basket.product.quantity', null);

        //Session::forget('basket');

        if($offerIds != null && $offerQty != null && count($offerIds) == count($offerQty)){

            $products = ProductModel::whereIn('id', $offerIds)->get();    
            
            if($products){
                $products->each(function($offer) use ($offerQty){
                    $this->products[$offer->id] = [
                        'id' => $offer->id,
                        'name' => $offer->name,
                        'price' => ProductModel::find($offer['id'])->getActialPriceAttribute(),
                        'total_price' => ProductModel::find($offer['id'])->getActialPriceAttribute() * Session::get('basket.product.quantity.' .$offer['id']),
                        'category' => $offer->category->name,
                        'img' => count($offer->images) ? $offer->images[0]->path : '',
                        'qty' => Session::get('basket.product.quantity.' .$offer['id']),
                        'size' => Session::get('basket.product.size.' .$offer['id']) ? Session::get('basket.product.size.' .$offer['id']) :  $offer->sizes[0]->name,
                        'color' => Session::get('basket.product.color.' .$offer['id']) ? Session::get('basket.product.color.' .$offer['id']) : $offer->colors[0]->name,
                    ];
                });
    
                $this->deliveryId = (int)Session::get('deliveryId');
                $this->paymentId = (int)Session::get('payment_Id');
                $this->deliveryPrice = (int)Session::get('deliveryPrice');
                $this->getTotalPrice();
                $this->cartCount = count($offerIds);
                $this->deliveryTotalPrice = $this->deliveryPrice + $this->totalPrice;
                Session::forget('deliveryPrice');
            }

            $methods = DeliveryMethod::all();
            if(Session::get('array_delivery')):
                foreach(Session::get('array_delivery') as $item):
                    $this->newMethods[$item['id']] = [
                        'id' => $item['id'],
                        'name' => $item['name'],
                        'comment' => $item['comment'],
                        'price_change' => $item['price_change'],
                        'class_name' => $item['class_name'],
                    ];
                endforeach;
            else:
                $methods->each(function($method){
                    $this->newMethods[$method->id] = [
                        'id' => $method->id,
                        'name' => $method->name,
                        'comment' => $method->comment,
                        'price_change' => $method->price_change,
                        'class_name' => $method->class_name,
                    ];
                });
            endif;

        }
    }

    


    public function onSendPromoCode(){
        $getCode = Input::get('promo_code');
        $promoCodeModel = PromoCode::all();
        

        if(!Session::get('promocode')):
            foreach($promoCodeModel as $code):

                if($getCode == $code->code && $code->enabled == 1):
                    if($code->type == 1): // на заказ
                        if($code->percent != 100):
                            Session::put('promo_percent', $code->percent);
                        endif;
                        Session::put('promocode', $code->id);
                    elseif($code->type == 0): // на доставку
                        $methods = DeliveryMethod::all();
                        $methods->each(function($method) use ($code){
                            $this->newMethods[$method->id] = [
                                'id' => $method->id,
                                'name' => $method->name,
                                'comment' => $method->comment,
                                'price_change' => ($code->percent != 100)
                                    ? $method->price_change * $code->percent / 100
                                    : '0',
                                'class_name' => $method->class_name,
                            ];
                        });
                        Session::put('promocode', $code->id);
                    endif;
                endif;
    
            endforeach;
        else:
            return[
                '#none-promo' => $this->renderPartial('none-promo', ['item' => 'Можно использовать только один промокод!'])]
            ;
        endif;

        Session::put('array_delivery', $this->newMethods);

        if($this->newMethods){
            return 
                ['#vladimir' => $this->renderPartial('delivery-methods', ['methods' => $this->newMethods])];
        }
        else{
            $this->onRun();
            return $this->refreshPartials();
        }
    }


    public function getTotalPrice(){        
        if($this->products){
            foreach($this->products as $product){
                $price_product = $product['qty'] * $product['price'];
                $this->totalPrice += $price_product;
            }
            if(Session::get('promo_percent') != 0):
                $this->totalPrice = $this->totalPrice * Session::get('promo_percent') / 100;
            endif;
        }
    }


    public function onPlusCart(){
       $id = Input::get('id');
       $qty_product = Session::get('basket.product.quantity.' .$id);

       Session::put('basket.product.quantity.' . $id, $qty_product + 1);

       $this->onRun();
       return $this->refreshPartials();
    }

    public function onRemoveProduct(){
        $id = Input::get('id');

        Session::forget('basket.product.offer_id.' . $id);
        Session::forget('basket.product.quantity.' . $id);

        $this->onRun();
        return $this->refreshPartials();
    }


    public function onMinusCart(){
       $id = Input::get('id');
       $qty_product = Session::get('basket.product.quantity.' .$id);
       

        if( Session::get('basket.product.quantity.' . $id) > 1 ){
            Session::put('basket.product.quantity.' . $id, $qty_product - 1);
        }

       $this->onRun();
       return $this->refreshPartials();
    }

    public function getProducts(){
        return ProductModel::all();
    }

    public function icrQty($arr){
        return Session::get('basket.product.quantity.'.$arr['id']) + 1;
    }


    public function onAddCartProduct(){

        $data = [
            'id' => Input::get('id', null),
            'qty' => Input::get('qty', 1)
        ];

        $d = ProductModel::find($data['id'])->with('images')->get();



        if (!Session::has('basket.product.offer_id.' . $data['id'])) {
            Session::put('basket.product.offer_id.' . $data['id'], $data['id']);
            Session::put('basket.product.quantity.' . $data['id'], $data['qty']);
        }
        else{
                Session::put('basket.product.quantity.' . $data['id'], $this->icrQty($data));
        }

        $this->onRun();
        return $this->refreshPartials();
    }



    public function onRefreshPartials(){
        $this->onRun();

        return $this->refreshPartials();
    }

    private function clearBasket(){
        $this->products = null;
        $this->totalPrice = 0;

        Session::forget('basket.product.offer_id');
        Session::forget('basket.product.quantity');
    }

    private function refreshPartials(array $partial = []){
        return array_merge([
            '.co-count' => $this->renderPartial('cart/_header'),
            '.cart-item' => $this->renderPartial('cart/_cart-item'),
            '.total-price' => $this->renderPartial('cart/_total_price'),
        ], $partial);
    }


    public function onSendPriceChange(){
        Session::put('deliveryId', Input::get('delivery_id'));
        Session::put('deliveryPrice', Input::get('delivery_price'));

        $this->onRun();

        return array_merge([
            '.d-price' => $this->renderPartial('delivery/delivery-price'),
            '.d-total-price' => $this->renderPartial('delivery/delivery-total-price'),
        ], []);
    }

    public function onSendPayment(){
        Session::put('payment_id', Input::get('method_id'));
        $this->onRun();
    }


    
    public function onCheckOut(){
        $this->onRun();

        $data = [
            'name' => Input::get('name', null),
            'phone' => Input::get('phone', null),
            'email' => Input::get('email', null),
            'address' => Input::get('address', null),
            'city' => Input::get('city', null),
            'phone' => Input::get('phone', null),
            'comment' => Input::get('comment', null),
            'post_code' => Input::get('post_code', null),
        ];

        $client = new ClientModel();

        $exists_client = ClientModel::where('phone', '=', $data['phone'])->first();

        if($exists_client != null){
            $order = new Order();
            $order->client_id = $exists_client->id;
            $order->address = $exists_client->address;
            $order->post_code = $data['post_code'];
            $order->comment = $data['comment'];
            $order->contents = $this->products;
            $order->delivery_method_id = ((int)$this->deliveryId) ? (int)$this->deliveryId : 3;
            $order->payment_method_id = ((int)$this->paymentId) ? (int)$this->paymentId : 1; 
            $order->promo_code = (Session::get('promocode')) ? Session::get('promocode') : '0';
    
            $order->total_price = $this->totalPrice;
            $order->total_price += $order->delivery_method->price_change;
    
            $order->save();
        }
        else{
            $client->name = $data['name'];
            $client->phone = $data['phone'];
            $client->address = $data['address'];
            $client->city = $data['city'];
            $client->index = $data['post_code'];
            $client->email = $data['email'];

            $client->save();


            $order = new Order();
            $order->client_id = $client->id;
            $order->address = $data['address'];
            $order->post_code = $data['post_code'];
            $order->comment = $data['comment'];
            $order->contents = $this->products;
            $order->delivery_method_id = ((int)$this->deliveryId) ? (int)$this->deliveryId : 3;
            $order->payment_method_id = ((int)$this->paymentId) ? (int)$this->paymentId : 1; 
            $order->promo_code = (Session::get('promocode')) ? Session::get('promocode') : '0';
    
            $order->total_price = $this->totalPrice;
            $order->total_price += $order->delivery_method->price_change;
    
            $order->save();
        }


        // очистить корзину и способ оплаты
        Session::forget('basket');
        Session::forget('deliveryId');
        Session::forget('method_id');
        Session::forget('promo_percent');
        Session::forget('array_delivery');
        Session::forget('promocode');

        $this->onRun();
        return $this->refreshPartials();
    }


    // Отравка размер и цвета в продукт

    public function onSendSize(){
            Session::put('basket.product.size.' . Input::get('id'), Input::get('size'));
            $this->onRun();
    }

    public function onSendColor(){
        Session::put('basket.product.color.' . Input::get('id'), Input::get('color'));
        $this->onRun();
    }

}

