<?php namespace Snapix\Catalog\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use  \Snapix\Catalog\Models\Product as ProductModel;


class RecentProduct extends ComponentBase
{

    public $productRecent = [];

    public function componentDetails()
    {
        return [
            'name'        => 'RecentProduct Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onSendRecentProduct(){

        $data = [
            'id' => Input::get('product_id', null),
            'date' => Input::get('date', null),
        ];
        
        if (!Session::has('recent.id.' . $data['id'])) {
            Session::put('recent.id.'. $data['id'], $data['id']);
            Session::put('recent.date.'. $data['id'], $data['date']);
        }

        $this->onRun();
        return $this->refreshPartials();
       
    }

    public function onRun(){
        //Session::forget('recent');

        $recentIds = Session::get('recent.id', null);
        $recentDates = Session::get('recent.date', null);

       if ($recentIds){

            $recent = ProductModel::whereIn('id', $recentIds)->get();

            if($recent){
                $recent->each(function($offer) use ($recentDates){
                    $this->productRecent[$offer->id] = [
                        'id' => $offer->id,
                        'name' => $offer->name,
                        'price' => $offer->price,
                        'discount_price' => $offer->discount_price,
                        'category' => $offer->category->name,
                        'img' => count($offer->images) ? $offer->images[0]->path : '',
                        'date' => Session::get('recent.date.' .$offer['id']),
                        'size' => $offer->sizes,
                        'slug' => $offer->slug
                    ];
                });

            }
       }
     
    }

    private function refreshPartials(array $partial = [])
    {
        return array_merge([
            '.recent-partial-render'  => $this->renderPartial('recent_product'),
        ], $partial);
    }
}
