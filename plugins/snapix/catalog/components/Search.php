<?php namespace Snapix\Catalog\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Snapix\Catalog\Models\Product as ProductModel;

class Search extends ComponentBase
{
    public $products;
    public $search;
    public $data_prices = [];
    public $min_max = [];

    public function componentDetails()
    {
        return [
            'name'        => 'Search Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }


    public function getMaxPrice()
    {
        return ProductModel::max('price');
    }

    public function getMinPrice()
    {
        return ProductModel::min('price');
    }

   


    public function onSendAmountPrice()
    {
        $this->search = Input::get('search');
        Session::put('min', Input::get('min'));
        Session::put('max', Input::get('max'));

        $this->onRun();
        return $this->refreshPartials();
    }


    public function onSendSearchForm()
    {
        $this->search = Input::get('search');

        $this->onRun();
        return $this->refreshPartials();

    }


    public function onRun(){
        $this->data_prices['min'] = (Session::get('min')) ? Session::get('min') : $this->getMinPrice();
        $this->data_prices['max'] = (Session::get('max')) ? Session::get('max') : $this->getMaxPrice();
        
        $this->min_max['max'] = $this->getMaxPrice();
        $this->min_max['min'] = $this->getMinPrice();


        if($this->search){
            $this->products = ProductModel::where(
                [
                    [ 'name', 'like', '%'. $this->search .'%'],
                    [ 'price', '>=', (Session::get('min')) ? Session::get('min') : $this->getMinPrice() ],
                    [ 'price', '<=', (Session::get('max')) ? Session::get('max') : $this->getMaxPrice() ],
                ]
            )->get();
        }
        else{
            $this->products = 0;
        }
    }


    private function refreshPartials(array $partial = [])
    {
        return array_merge([
            '#result_search' => $this->renderPartial('search_container', ['res' => $this->products]),
        ], $partial);
    }


}
