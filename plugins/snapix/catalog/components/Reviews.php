<?php namespace Snapix\Catalog\Components;


use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use  \Snapix\Catalog\Models\Product as ProductModel;
use \Snapix\Catalog\Models\Review as ReviewModel;


class Reviews extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Reviews Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }


    public function onSendReview(){

        $review = new ReviewModel();

        $review->author_name = Input::get('author_name');
        $review->author_email = Input::get('author_email');
        $review->author_msg = Input::get('author_msg');
        $review->product_id = Input::get('product_id');
        $review->product_slug = Input::get('product_slug');

        

        $review->save();
    }
}
