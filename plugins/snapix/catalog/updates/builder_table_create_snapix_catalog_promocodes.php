<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSnapixCatalogPromocodes extends Migration
{
    public function up()
    {
        Schema::create('snapix_catalog_promocodes', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('code', 191);
            $table->smallInteger('enabled')->unsigned();
            $table->integer('percent')->unsigned();
            $table->smallInteger('type')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('snapix_catalog_promocodes');
    }
}
