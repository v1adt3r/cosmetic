<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSnapixCatalogDeliveryMethods extends Migration
{
    public function up()
    {
        Schema::create('snapix_catalog_delivery_methods', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 191);
            $table->string('comment', 191);
            $table->integer('price_change')->unsigned()->default(0);
            $table->string('class_name', 191);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('snapix_catalog_delivery_methods');
    }
}
