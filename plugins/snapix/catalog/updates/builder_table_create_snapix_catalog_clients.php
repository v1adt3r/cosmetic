<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSnapixCatalogClients extends Migration
{
    public function up()
    {
        Schema::create('snapix_catalog_clients', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 191);
            $table->string('phone', 191);
            $table->string('address', 191);
            $table->string('city', 191);
            $table->string('index', 191);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('snapix_catalog_clients');
    }
}
