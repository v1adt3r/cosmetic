<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSnapixCatalogProducts extends Migration
{
    public function up()
    {
        Schema::table('snapix_catalog_products', function($table)
        {
            $table->smallInteger('discount_price')->unsigned()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('snapix_catalog_products', function($table)
        {
            $table->dropColumn('discount_price');
        });
    }
}
