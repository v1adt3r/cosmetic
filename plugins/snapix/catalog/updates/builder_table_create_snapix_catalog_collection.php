<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSnapixCatalogCollection extends Migration
{
    public function up()
    {
        Schema::create('snapix_catalog_collection', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 191);
            $table->text('description');
            $table->integer('collection_category_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->string('slug', 191);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('snapix_catalog_collection');
    }
}
