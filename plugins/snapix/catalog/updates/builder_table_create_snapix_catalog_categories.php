<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSnapixCatalogCategories extends Migration
{
    public function up()
    {
        Schema::create('snapix_catalog_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 191);
            $table->text('description');
            $table->string('slug', 191);
            $table->smallInteger('parent_category_id')->unsigned();
            $table->smallInteger('enabled');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('snapix_catalog_categories');
    }
}
