<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteSnapixCatalogPositionProduct extends Migration
{
    public function up()
    {
        Schema::dropIfExists('snapix_catalog_position_product');
    }
    
    public function down()
    {
        Schema::create('snapix_catalog_position_product', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('product_id');
            $table->integer('position_id');
            $table->primary(['product_id','position_id']);
        });
    }
}
