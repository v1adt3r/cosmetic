<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSnapixCatalogColors extends Migration
{
    public function up()
    {
        Schema::create('snapix_catalog_colors', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 51);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('snapix_catalog_colors');
    }
}
