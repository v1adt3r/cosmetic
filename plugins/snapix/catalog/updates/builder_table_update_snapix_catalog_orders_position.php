<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSnapixCatalogOrdersPosition extends Migration
{
    public function up()
    {
        Schema::table('snapix_catalog_orders_position', function($table)
        {
            $table->string('item_name', 191);
        });
    }
    
    public function down()
    {
        Schema::table('snapix_catalog_orders_position', function($table)
        {
            $table->dropColumn('item_name');
        });
    }
}
