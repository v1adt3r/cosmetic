<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateParentCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('snapix_catalog_parent_categories', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('slug', 191);
            $table->smallInteger('enabled');
            $table->text('description');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('snapix_catalog_parent_categories');
    }
}
