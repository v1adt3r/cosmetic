<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSnapixCatalogOrders extends Migration
{
    public function up()
    {
        Schema::table('snapix_catalog_orders', function($table)
        {
            $table->integer('total_price')->unsigned()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('snapix_catalog_orders', function($table)
        {
            $table->dropColumn('total_price');
        });
    }
}
