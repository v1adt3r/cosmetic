<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSnapixCatalogReviews extends Migration
{
    public function up()
    {
        Schema::table('snapix_catalog_reviews', function($table)
        {
            $table->string('product_slug', 191);
        });
    }
    
    public function down()
    {
        Schema::table('snapix_catalog_reviews', function($table)
        {
            $table->dropColumn('product_slug');
        });
    }
}
