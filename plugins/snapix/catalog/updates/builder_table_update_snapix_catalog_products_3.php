<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSnapixCatalogProducts3 extends Migration
{
    public function up()
    {
        Schema::table('snapix_catalog_products', function($table)
        {
            $table->integer('qty')->unsigned()->default(1);
        });
    }
    
    public function down()
    {
        Schema::table('snapix_catalog_products', function($table)
        {
            $table->dropColumn('qty');
        });
    }
}
