<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSnapixCatalogOrderProduct extends Migration
{
    public function up()
    {
        Schema::create('snapix_catalog_order_product', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('order_id');
            $table->integer('product_id');
            $table->integer('total_quantity')->default(1);
            $table->integer('total_price')->default(0);
            $table->primary(['order_id','product_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('snapix_catalog_order_product');
    }
}
