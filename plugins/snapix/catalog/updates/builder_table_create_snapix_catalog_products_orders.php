<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSnapixCatalogProductsOrders extends Migration
{
    public function up()
    {
        Schema::create('snapix_catalog_products_orders', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('product_id');
            $table->integer('order_id');
            $table->primary(['product_id','order_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('snapix_catalog_products_orders');
    }
}