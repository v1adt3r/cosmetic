<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSnapixCatalogPaymentMethod extends Migration
{
    public function up()
    {
        Schema::create('snapix_catalog_payment_method', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 191);
            $table->string('comment', 191);
            $table->string('class_name', 191);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('snapix_catalog_payment_method');
    }
}
