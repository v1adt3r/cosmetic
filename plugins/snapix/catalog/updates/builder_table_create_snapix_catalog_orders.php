<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSnapixCatalogOrders extends Migration
{
    public function up()
    {
        Schema::create('snapix_catalog_orders', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->smallInteger('client_id')->nullable()->unsigned();
            $table->string('address', 191)->nullable();
            $table->string('post_code', 191)->nullable();
            $table->text('comment')->nullable();
            $table->text('contents')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->dateTime('delivery_date')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('delivery_method_id')->unsigned();
            $table->string('promo_code', 51);
            $table->integer('payment_method_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('snapix_catalog_orders');
    }
}
