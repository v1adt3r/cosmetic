<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteSnapixCatalogProductsOrders extends Migration
{
    public function up()
    {
        Schema::dropIfExists('snapix_catalog_products_orders');
    }
    
    public function down()
    {
        Schema::create('snapix_catalog_products_orders', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('product_id');
            $table->integer('order_id');
            $table->primary(['product_id','order_id']);
        });
    }
}
