<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteSnapixCatalogOrderProduct extends Migration
{
    public function up()
    {
        Schema::dropIfExists('snapix_catalog_order_product');
    }
    
    public function down()
    {
        Schema::create('snapix_catalog_order_product', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('order_id');
            $table->integer('product_id');
            $table->integer('total_quantity')->default(1);
            $table->integer('total_price')->default(0);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->primary(['order_id','product_id']);
        });
    }
}
