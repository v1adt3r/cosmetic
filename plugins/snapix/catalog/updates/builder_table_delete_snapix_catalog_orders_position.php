<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteSnapixCatalogOrdersPosition extends Migration
{
    public function up()
    {
        Schema::dropIfExists('snapix_catalog_orders_position');
    }
    
    public function down()
    {
        Schema::create('snapix_catalog_orders_position', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('order_id')->unsigned();
            $table->integer('item_id')->unsigned();
            $table->decimal('price', 8, 2)->nullable();
            $table->integer('quantity')->nullable()->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
}
