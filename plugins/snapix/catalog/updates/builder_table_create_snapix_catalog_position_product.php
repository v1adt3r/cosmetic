<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSnapixCatalogPositionProduct extends Migration
{
    public function up()
    {
        Schema::create('snapix_catalog_position_product', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('product_id');
            $table->integer('position_id');
            $table->primary(['product_id','position_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('snapix_catalog_position_product');
    }
}
