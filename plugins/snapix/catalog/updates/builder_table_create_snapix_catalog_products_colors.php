<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSnapixCatalogProductsColors extends Migration
{
    public function up()
    {
        Schema::create('snapix_catalog_products_colors', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('product_id');
            $table->integer('color_id');
            $table->primary(['product_id','color_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('snapix_catalog_products_colors');
    }
}
