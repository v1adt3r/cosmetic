<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSnapixCatalogProducts4 extends Migration
{
    public function up()
    {
        Schema::table('snapix_catalog_products', function($table)
        {
            $table->dropColumn('order_id');
        });
    }
    
    public function down()
    {
        Schema::table('snapix_catalog_products', function($table)
        {
            $table->smallInteger('order_id')->unsigned();
        });
    }
}
