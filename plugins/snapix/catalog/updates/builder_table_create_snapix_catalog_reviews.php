<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSnapixCatalogReviews extends Migration
{
    public function up()
    {
        Schema::create('snapix_catalog_reviews', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('author_name', 191);
            $table->string('author_email', 191);
            $table->text('author_msg');
            $table->smallInteger('enabled')->default(0);
            $table->integer('product_id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('snapix_catalog_reviews');
    }
}