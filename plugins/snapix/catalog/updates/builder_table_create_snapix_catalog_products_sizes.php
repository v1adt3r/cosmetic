<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSnapixCatalogProductsSizes extends Migration
{
    public function up()
    {
        Schema::create('snapix_catalog_products_sizes', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('size_id');
            $table->integer('product_id');
            $table->primary(['size_id','product_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('snapix_catalog_products_sizes');
    }
}
