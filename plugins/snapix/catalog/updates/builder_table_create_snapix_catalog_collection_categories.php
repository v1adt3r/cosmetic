<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSnapixCatalogCollectionCategories extends Migration
{
    public function up()
    {
        Schema::create('snapix_catalog_collection_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 191);
            $table->string('slug', 191);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('snapix_catalog_collection_categories');
    }
}
