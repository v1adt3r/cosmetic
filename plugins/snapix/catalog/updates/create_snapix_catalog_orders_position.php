<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateSnapixCatalogOrdersPosition extends Migration
{
    public function up()
    {
        Schema::create('snapix_catalog_orders_position', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('order_id')->unsigned();
            $table->integer('item_id')->unsigned();
            $table->decimal('price')->nullable();
            $table->integer('quantity')->unsigned()->nullable();
            $table->timestamps();

            $table->index('item_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('snapix_catalog_orders_position');
    }
}
