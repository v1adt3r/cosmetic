<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSnapixCatalogClients extends Migration
{
    public function up()
    {
        Schema::table('snapix_catalog_clients', function($table)
        {
            $table->string('email', 191);
        });
    }
    
    public function down()
    {
        Schema::table('snapix_catalog_clients', function($table)
        {
            $table->dropColumn('email');
        });
    }
}
