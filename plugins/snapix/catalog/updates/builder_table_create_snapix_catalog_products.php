<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSnapixCatalogProducts extends Migration
{
    public function up()
    {
        Schema::create('snapix_catalog_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 191);
            $table->text('description');
            $table->text('care');
            $table->text('composition');
            $table->string('slug', 191);
            $table->smallInteger('price')->unsigned();
            $table->smallInteger('category_id')->unsigned();
            $table->smallInteger('enabled');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('articul', 91);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('snapix_catalog_products');
    }
}
