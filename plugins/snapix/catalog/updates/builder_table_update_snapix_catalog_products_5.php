<?php namespace Snapix\Catalog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSnapixCatalogProducts5 extends Migration
{
    public function up()
    {
        Schema::table('snapix_catalog_products', function($table)
        {
            $table->renameColumn('qty', 'total_quantity');
        });
    }
    
    public function down()
    {
        Schema::table('snapix_catalog_products', function($table)
        {
            $table->renameColumn('total_quantity', 'qty');
        });
    }
}
