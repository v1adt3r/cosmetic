<?php namespace Snapix\Catalog;

use System\Classes\PluginBase;
use Event;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
      return [
        '\Snapix\Catalog\Components\Basket' => 'Basket',
        '\Snapix\Catalog\Components\Reviews' => 'Reviews',
        '\Snapix\Catalog\Components\RecentProduct' => 'RecentProduct',
        '\Snapix\Catalog\Components\Search' => 'Search',
      ];
    }

    public function pluginDetails()
    {
        return [
            'name'        => 'catalog',
            'description' => 'No description provided yet...',
            'author'      => 'snapix',
            'icon'        => 'icon-leaf'
        ];
    }

    public function registerSettings()
    {
    }

    public function registerNavigation(){
      return [
        'catalog' => [
          'label' => 'Магазин',
          'url' => \Backend::url('snapix\catalog\products'),
          'iconSvg'  => 'plugins/snapix/catalog/assets/images/cart.png',
  
          'sideMenu' => [
            'products' => [
              'label' => 'Продукты',
              'iconSvg'  => 'plugins/snapix/catalog/assets/images/dress.png',
              'url'   => \Backend::url('snapix/catalog/products'),
            ],
            'parentcategories'  => [
              'label' => 'Главная категория',
              'iconSvg'  => 'plugins/snapix/catalog/assets/images/diagram.png',
              'url'   => \Backend::url('snapix/catalog/parentcategories'),
            ],
            'categories'  => [
              'label' => 'Категории',
              'iconSvg'  => 'plugins/snapix/catalog/assets/images/diagram.png',
              'url'   => \Backend::url('snapix/catalog/categories'),
            ],
            'colors'  => [
              'label' => 'Цвета',
              'iconSvg'  => 'plugins/snapix/catalog/assets/images/rgb.png',
              'url'   => \Backend::url('snapix/catalog/colors'),
            ],
            'sizes'  => [
              'label' => 'Размеры',
              'iconSvg'  => 'plugins/snapix/catalog/assets/images/meter.png',
              'url'   => \Backend::url('snapix/catalog/sizes'),
            ],
            'orders'  => [
              'label' => 'Заказы',
              'iconSvg'  => 'plugins/snapix/catalog/assets/images/shopping-bag.png',
              'url'   => \Backend::url('snapix/catalog/orders'),
            ],
            'clients'  => [
              'label' => 'Клиенты',
              'iconSvg'  => 'plugins/snapix/catalog/assets/images/teamwork.png',
              'url'   => \Backend::url('snapix/catalog/clients'),
            ],
            'deliverymethods'  => [
              'label' => 'Способ доставки',
              'iconSvg'  => 'plugins/snapix/catalog/assets/images/delivery-truck.png',
              'url'   => \Backend::url('snapix/catalog/deliverymethods'),
            ],
            'paymentmethods'  => [
              'label' => 'Способ оплаты',
              'iconSvg'  => 'plugins/snapix/catalog/assets/images/debit-card.png',
              'url'   => \Backend::url('snapix/catalog/paymentmethods'),
            ],
            'promocodes'  => [
              'label' => 'Промо коды',
              'iconSvg'  => 'plugins/snapix/catalog/assets/images/debit-card.png',
              'url'   => \Backend::url('snapix/catalog/promocodes'),
            ],
            'collections'  => [
              'label' => 'Коллекции',
              'iconSvg'  => 'plugins/snapix/catalog/assets/images/shopping-online.png',
              'url'   => \Backend::url('snapix/catalog/collections'),
            ],
            'collectioncategories'  => [
              'label' => 'Категории коллекий',
              'iconSvg'  => 'plugins/snapix/catalog/assets/images/list.png',
              'url'   => \Backend::url('snapix/catalog/collectioncategories'),
            ],
            'reviews'  => [
              'label' => 'Отзывы',
              'iconSvg'  => 'plugins/snapix/catalog/assets/images/review.png',
              'url'   => \Backend::url('snapix/catalog/reviews'),
            ],
          ],
        ],
      ];
    }

    public function registerFormWidgets()
    {
        return [
            'Snapix\Catalog\FormWidgets\Optionbox' => [
                'label' => 'Optionbox field',
                'code' => 'optionbox'
            ]
        ];
    }



    public function registerListColumnTypes()
    {
        return [
            'uppercase' => [$this, 'evalUppercaseListColumn'],
            'category' => [$this, 'evalCategoryColumn'],
        ];
    }


    public function evalUppercaseListColumn($value, $column, $record)
    {
        return $value['name'];
    }
    
    public function evalCategoryColumn($value, $column, $record){
      return $value['name'];
    }




}
