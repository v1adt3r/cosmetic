<?php namespace Snapix\Catalog\Models;

use Model;

/**
 * Category Model
 */
class Category extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'snapix_catalog_categories';
    public $timestamps = false;


    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */

    // Category Model
    public $hasMany = [
        'products' => [
            'Snapix\Catalog\Models\Product',
            'key' => 'category_id',
        ],
    ];

    public $belongsTo = [
        'parent_category' => \Snapix\Catalog\Models\ParentCategory::class,
    ];

    public $attachMany = [
        'images' => 'System\Models\File'
    ];
}
