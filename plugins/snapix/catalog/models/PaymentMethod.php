<?php namespace Snapix\Catalog\Models;

use Model;

/**
 * Product Model
 */
class PaymentMethod extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'snapix_catalog_payment_method';
    public $timestamps = false;

    
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasMany = [
        'order' => \Snapix\Catalog\Models\Order::class,
        'key' => 'id'
    ];

}
