<?php namespace Snapix\Catalog\Models;

use Model;

/**
 * ParentCategory Model
 */
class ParentCategory extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'snapix_catalog_parent_categories';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasMany = [
        'child_category' => [
            'Snapix\Catalog\Models\Category',
            'key' => 'parent_category_id',
        ],
    ];
}
