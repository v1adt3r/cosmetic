<?php namespace Snapix\Catalog\Models;

use Model;

/**
 * Client Model
 */
class Client extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'snapix_catalog_clients';
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    
    public $hasMany = [
        'orders' => [
            'Snapix\Catalog\Models\Order',
            'key' => 'client_id',
        ],
    ];



   
}
