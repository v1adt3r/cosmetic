<?php namespace Snapix\Catalog\Models;

use Model;

/**
 * Product Model
 */
class PromoCode extends Model
{

    public $cost;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'snapix_catalog_promocodes';
    public $timestamps = false;

    
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
 
    public function getTypeOptions()
    {
        $options = ['Доставка', 'Заказ'];

        return $options;
    }

}
