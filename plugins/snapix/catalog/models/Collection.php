<?php namespace Snapix\Catalog\Models;

use Model;

/**
 * Product Model
 */
class Collection extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'snapix_catalog_collection';
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'collection_category' => \Snapix\Catalog\Models\CollectionCategory::class,
        'category' => \Snapix\Catalog\Models\Category::class,
    ];

    public $attachMany = [
        'images' => 'System\Models\File',
    ];

}
