<?php namespace Snapix\Catalog\Models;

use Model;

/**
 * Product Model
 */
class Review extends Model
{

    public $cost;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'snapix_catalog_reviews';

    
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'product' => [
            'Snapix\Catalog\Models\Product',
            'key' => 'id',
        ],
    ];

}
