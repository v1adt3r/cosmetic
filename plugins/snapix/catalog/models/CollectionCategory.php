<?php namespace Snapix\Catalog\Models;

use Model;

/**
 * Product Model
 */
class CollectionCategory extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'snapix_catalog_collection_categories';
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];
 
    
    /**
     * @var array Relations
     */

    public $hasMany = [
        'collections' => \Snapix\Catalog\Models\Collection::class,
    ];

   
    public $attachMany = [
        'images' => 'System\Models\File',
    ];
}
