<?php namespace Snapix\Catalog\Models;

use Model;

/**
 * Product Model
 */
class Size extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'snapix_catalog_sizes';
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */

    public $belongsToMany = [
        'product' => [
            'Snapix\Catalog\Models\Product',
            'table' => 'snapix_catalog_products_sizes',
        ],
    ];

}
