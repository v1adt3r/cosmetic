<?php namespace Snapix\Catalog\Models;

use Model;

/**
 * Product Model
 */
class DeliveryMethod extends Model
{

    public $cost;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'snapix_catalog_delivery_methods';
    public $timestamps = false;

    
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasMany = [
        'order' => \Snapix\Catalog\Models\Order::class,
        'key' => 'id'
    ];

}
