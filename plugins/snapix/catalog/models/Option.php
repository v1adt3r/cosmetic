<?php namespace Snapix\Catalog\Models;

use Model;

/**
 * Option Model
 */
class Option extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'snapix_catalog_options';
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];


    // Option Model
    public $hasMany = [
        'options' => \Snapix\Catalog\Models\OptionValue::class,
        'key' => 'id'
    ];

    public $belongsToMany = [
        'product' => [
            'Snapix\Catalog\Models\Product',
            'table' => 'snapix_catalog_option_products',
        ],
    ];

    public $belongsTo = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
