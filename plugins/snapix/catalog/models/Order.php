<?php namespace Snapix\Catalog\Models;

use Model;
use RainLab\User\Models\User;

/**
 * Model
 */
class Order extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'snapix_catalog_orders';

    protected $jsonable = ['contents'];

    /**
     * @var array Validation rules
     */
    public $belongsTo = [
        'client' => \Snapix\Catalog\Models\Client::class,
        'delivery_method' => \Snapix\Catalog\Models\DeliveryMethod::class,
        'payment_method' => \Snapix\Catalog\Models\PaymentMethod::class,
    ];

    public $rules = [
    ];
    

    public function beforeSave(){
        $this->delivery_method_id = ($this->delivery_method_id) ? $this->delivery_method_id : 1;
        $this->address = ($this->client->address) ? $this->client->address : '-';
        $this->post_code = ($this->client->index) ? $this->client->index : '-';

    }

}
