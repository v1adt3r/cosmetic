<?php namespace Snapix\Catalog\Models;

use Model;

/**
 * Product Model
 */
class Color extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'snapix_catalog_colors';
    public $timestamps = false;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];
    
    /**
     * @var array Relations
     */
    // Color Model
    public $belongsToMany = [
        'product' => [
            'Snapix\Catalog\Models\Product',
            'table' => 'snapix_catalog_products_colors',
        ],
    ];

    public $attachMany = [
        'images' => 'System\Models\File',
    ];
    

}
