<?php namespace Snapix\Catalog\Models;

use Model;

/**
 * OptionValue Model
 */
class OptionValue extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'snapix_catalog_option_values';
    public $timestamps = false;
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    
    public $belongsTo = [
        'option' => \Snapix\Catalog\Models\Option::class,
    ];

    
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
