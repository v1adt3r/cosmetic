<?php namespace Snapix\Catalog\Models;

use Model;

/**
 * Product Model
 */
class Product extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'snapix_catalog_products';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];
    protected $jsonable = ['contents'];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'category' => \Snapix\Catalog\Models\Category::class,
    ];

    public $attachMany = [
        'images' => 'System\Models\File',
        'video'  => 'System\Models\File'
    ];

    public $hasMany = [    
        'review' => [
            'Snapix\Catalog\Models\Review',
            'key' => 'product_id',
        ],
    ];



    public $belongsToMany = [
        'colors' => [
            'Snapix\Catalog\Models\Color',
            'table' => 'snapix_catalog_products_colors',
        ],
        'sizes' => [
            'Snapix\Catalog\Models\Size',
            'table' => 'snapix_catalog_products_sizes',
        ],
    ];

    public function getDiscountAttribute(){
        $discount = $this->price - $this->discount_price;
        $interest = $discount / $this->price;
        $res = $interest * 100;

        return ((int)$res == 100) ? 0 : (int)$res;
    }

    public function getActialPriceAttribute(){
        return ($this->discount_price == 0) ? $this->price : $this->discount_price;
    }

}
