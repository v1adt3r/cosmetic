$(function () {

    var name_input =      $('.form-checkout input[name="name"]');
    var email_input =     $('.form-checkout input[name="email"]');
    var address_input =   $('.form-checkout input[name="address"]');
    var city_input =      $('.form-checkout input[name="city"]');
    var phone_input =     $('.form-checkout input[name="phone"]');
    var comment_input =   $('.form-checkout textarea[name="comment"]');
    var post_code_input = $('.form-checkout input[name="post_code"]');

    $('.adress-self').css('opacity', '0');
    $('.user-city').css('display', 'none');
    $('.user-index').css('display', 'none');
    $('.user-adres').css('display', 'none');
    $('.adress-self').css('opacity', '1');

    $('.checkout-two').css('display', 'none');

    $(document).on('click', '.click-method', function () {
        $('.delivery-click .delivery-circle').removeClass('active')
        $(this).find('.delivery-circle').addClass('active');

        var form = $('.form-checkout');

        switch ($(this).attr('data-name')) {
            case 'samovyvoz':
                $('.adress-self').css('opacity', '1');
                $('.user-city').css('display', 'none');
                $('.user-index').css('display', 'none');
                $('.user-adres').css('display', 'none');
                break;
            case 'pochta-rossii':
                $('.user-city').css('display', 'flex');
                $('.user-index').css('display', 'flex');
                $('.adress-self').css('opacity', '0');
                $('.user-adres').css('display', 'flex');
                break;
            case 'kurer':
                $('.user-city').css('display', 'none');
                $('.user-index').css('display', 'none');
                $('.user-adres').css('display', 'flex');
                $('.adress-self').css('opacity', '0');
                $(form).css({ opacity: 1 });
                break;
        }
    })


    $('.next-checkout').click(function () {
        if ($('.user-adres').css('display') == 'none') {
            if (name_input.val() == '' || email_input.val() == '') {
                alert_error_valid();
            }
            else {
                $('.checkout-one').css('display', 'none');
                $('.checkout-two').css('display', 'block');
                show_client_info();
            }
        }
        else {
            if (name_input.val() == '') {
                alert_error_valid();
            }
            else if (address_input.val() == '') {
                alert_error_valid();
            }
            else {
                $('.checkout-one').css('display', 'none');
                $('.checkout-two').css('display', 'block');
                show_client_info();
            }
        }
    })


    $('#checkout').click(function () {
       


            $.request('onCheckout', {
                data: {
                    name: name_input.val(),
                    phone:phone_input.val(),
                    email: email_input.val(),
                    address: address_input.val(),
                    city: city_input.val(),
                    phone: phone_input.val(),
                    comment: comment_input.val(),
                    post_code: post_code_input.val()
                },
            });

            $('#checkout_end').removeClass('none');

    })
})


$('.to_change').click(function () {
    $('.checkout-one').css('display', 'block');
    $('.checkout-two').css('display', 'none');
})


function show_client_info() {
    $('.client-name').html($('.form-checkout input[name="name"]').val());
    $('.client-phone').html($('.form-checkout input[name="phone"]').val());
    $('.client-address').html($('.form-checkout input[name="address"]').val());
    $('.client-city').html($('.form-checkout input[name="city"]').val());
    $('.client-post-code').html($('.form-checkout input[name="post_code"]').val());
    $('.client-email').html($('.form-checkout input[name="email"]').val());

}





function alert_error_valid() {
    $('#error-msg').text('Пожалуйста введите ваши данные');
    $('#error-msg').css('opacity', '1');
    setTimeout(function () {
        $('#error-msg').css('opacity', '0');
    }, 3000);
}